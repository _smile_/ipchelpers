include(ExternalProject)

set(DEPENDENCIES_PREFIX_PATH ${CMAKE_CURRENT_LIST_DIR}/../dependencies)
message(STATUS DEPENDENCIES_PREFIX_PATH=${DEPENDENCIES_PREFIX_PATH})

if(BOOST_BUILD_TOOLSET STREQUAL "")
    if(APPLE)
        set(BOOST_BUILD_TOOLSET clang)
    else()
        set(BOOST_BUILD_TOOLSET gcc)
    endif()
endif()

if (NOT EXISTS "${DEPENDENCIES_PREFIX_PATH}/boost/lib")
    set(BOOST_VERSION 1.79.0)
    set(BOOST_FILE boost_1_79_0.tar.gz)
    set(BOOST_CONFIGURE_COMMAND ./bootstrap.sh)
    set(BOOST_BUILD_COMMAND ./b2)

    message(STATUS "NOT EXISTS DIRECTORY")
    set(BOOST_URL "https://boostorg.jfrog.io/artifactory/main/release/${BOOST_VERSION}/source/${BOOST_FILE}")
    #set(BOOST_URL "${CMAKE_CURRENT_LIST_DIR}/${BOOST_FILE}")
    ExternalProject_Add(external_boost
            PREFIX ${DEPENDENCIES_PREFIX_PATH}/boost-tmp
            URL ${BOOST_URL}
            BUILD_IN_SOURCE 1
            CONFIGURE_COMMAND ${BOOST_CONFIGURE_COMMAND}
            --with-toolset=${BOOST_BUILD_TOOLSET}
            --with-libraries=regex
            link=static
            variant=release
            threading=multi
            runtime-link=static
            --prefix=<INSTALL_DIR>
            BUILD_COMMAND
            ${BOOST_BUILD_COMMAND} install
            INSTALL_COMMAND ""
            INSTALL_DIR ${DEPENDENCIES_PREFIX_PATH}/boost)
    list(APPEND DEPENDENCIES_LIST external_boost)
endif()