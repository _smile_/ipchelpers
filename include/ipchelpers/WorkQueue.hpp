#pragma once

#include <deque>
#include <functional>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include <thread>

template <typename F, typename... Args>
struct IsInvocable
    : std::is_constructible<std::function<void(Args ...)>,
                            std::reference_wrapper<typename std::remove_reference<F>::type>>
{};

class WorkQueue
{
public:
    using tTask = std::function<void()>;

public:
    WorkQueue();

    ~WorkQueue();
    template<typename TTask,
            class = typename std::enable_if<IsInvocable<TTask>::value>::type>
    void pushTask(TTask&& task)
    {
        std::unique_lock<std::mutex> lock(m_Mutex);
        m_Tasks.push_back(std::forward<TTask>(task));
        lock.unlock();
        m_ConditionVariable.notify_one();
    }

    bool start();
    bool stop();

private:
    tTask popTask();
    void threadFunction();

private:
    enum class State {
        RUN,
        STOPPING,
        STOPPED
    };
    std::mutex m_Mutex;
    std::condition_variable m_ConditionVariable;
    std::deque<tTask> m_Tasks;
    std::atomic<State> m_State;
    std::thread m_Thread;
};
