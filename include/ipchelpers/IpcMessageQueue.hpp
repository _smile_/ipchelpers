#include <memory>
#include <string>
#include <functional>

class IpcMessageQueue
{
public:
  IpcMessageQueue(std::string name,
                  std::function<void(char const* data, size_t size)>&& onReceive);

  void Receive();
  void TryReceive();
  void Send(char const* data, size_t size);
  static void RemoveQueue(std::string name);

private:
  class Impl;
  std::unique_ptr<Impl> _impl;
};