#include <ipchelpers/IpcMessageQueue.hpp>

#include <boost/interprocess/ipc/message_queue.hpp>

#include <vector>

class IpcMessageQueue::Impl
{
public:
  Impl(std::string const& name,
       std::function<void(char const* data, size_t size)>&& onReceive,
       int messageCountMax = 100,
       int messageSizeMax = 1024)
       : _onReceive{std::move(onReceive)}
       , _mq(boost::interprocess::open_or_create, name.c_str(), messageCountMax, messageSizeMax)
      , _data(messageSizeMax)
  {
  }

  void TryReceive()
  {
    unsigned int priority;
    boost::interprocess::message_queue::size_type recvd_size;
    _mq.try_receive(_data.data(), _data.size(), recvd_size, priority);
    if (recvd_size)
    {
      _onReceive(_data.data(), recvd_size);
    }
  }

  void Receive()
  {
    unsigned int priority;
    boost::interprocess::message_queue::size_type recvd_size;
    _mq.receive(_data.data(), _data.size(), recvd_size, priority);
    _onReceive(_data.data(), recvd_size);
  }

  void Send(char const* data, size_t size)
  {
    _mq.send(data, size, 0);
  }

  std::function<void(char const* data, size_t size)>&& _onReceive;
  boost::interprocess::message_queue _mq;
  std::vector<char> _data;
};

IpcMessageQueue::IpcMessageQueue(std::string name,
                                 std::function<void(char const* data, size_t size)>&& onReceive)
    : _impl{std::make_unique<Impl>(name, std::move(onReceive))}
{
}

void IpcMessageQueue::TryReceive()
{
  _impl->TryReceive();
}

void IpcMessageQueue::Receive()
{
  _impl->Receive();
}

void IpcMessageQueue::Send(char const* data, size_t size)
{
    _impl->Send(data, size);
}

void IpcMessageQueue::RemoveQueue(std::string name)
{
  boost::interprocess::message_queue::remove(name.c_str());
}

#if defined(IPC_MESSAGE_QUEUE_TEST_1)
#include <thread>
#include <chrono>

int main()
{
  IpcMessageQueue::RemoveQueue("test_queue");
  IpcMessageQueue ipcMessageQueue("test_queue", [&](char const* data, size_t size) {
    ipcMessageQueue.Send(data, size);
  });
  for (auto i = 0; i < 100; ++i)
  {
    ipcMessageQueue.Receive();
  }
  return 0;
}
#endif

#if defined(IPC_MESSAGE_QUEUE_TEST_2)
#include <thread>
#include <chrono>
#include <iostream>

int main()
{
  const char* UkraineItIsEurope = "Ще не вмерла Україна...";
  IpcMessageQueue ipcMessageQueue("test_queue", [&](char const* data, size_t size) {
    if ((strlen(UkraineItIsEurope) != size) ||
        (memcmp(UkraineItIsEurope, data, strlen(UkraineItIsEurope)) != 0))
    {
      throw std::runtime_error("От халепа!");
    }
    else
    {
      std::cout << "Ця штуковина працює!" << std::endl;
    }
  });
  for (auto i = 0; i < 100; ++i)
  {
    ipcMessageQueue.Send(UkraineItIsEurope, strlen(UkraineItIsEurope));
    ipcMessageQueue.Receive();
  }
  return 0;
}
#endif