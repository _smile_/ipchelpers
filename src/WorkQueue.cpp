#include <ipchelpers/WorkQueue.hpp>

WorkQueue::WorkQueue()
    : m_State(State::STOPPED)
{
}

WorkQueue::~WorkQueue()
{
    stop();
}

bool WorkQueue::start()
{
    if (m_State == State::STOPPED)
    {
        m_State = State::RUN;
        m_Thread = std::thread(std::bind(&WorkQueue::threadFunction, this));
        return true;
    }
    return false;
}

bool WorkQueue::stop()
{
    if (m_State == State::RUN)
    {
        m_State = State::STOPPING;
        m_ConditionVariable.notify_all();
        if ((m_Thread.joinable()) &&
            (std::this_thread::get_id() != m_Thread.get_id()))
        {
            m_Thread.join();
        }
        return true;
    }
    return false;
}

WorkQueue::tTask WorkQueue::popTask()
{
    tTask task{};

    std::unique_lock<std::mutex> lock(m_Mutex);
    m_ConditionVariable.wait(lock, [this]() {
        return !m_Tasks.empty() || (m_State == State::STOPPING);
    });

    if (m_Tasks.empty())
    {
        return task;
    }

    task = std::move(m_Tasks.front());
    m_Tasks.pop_front();

    return task;
}

void WorkQueue::threadFunction()
{
    while (true)
    {
        tTask t = popTask();
        if (!t)
        {
            m_State = State::STOPPED;
            return;
        }
        t();
    }
}

#if defined(WORK_QUEUE_UNITTEST)

#include <algorithm>
#include <iostream>
#include <future>
#include <sstream>
#include <array>

using tWorkQueueList = std::array<WorkQueue, 3>;

bool startAll(tWorkQueueList& workQueueList)
{
    bool result = true;
    auto startWorkQueue = [&](tWorkQueueList::reference workQueue) {
        result = result && workQueue.start();
    };
    std::for_each(workQueueList.begin(), workQueueList.end(), startWorkQueue);
    return result;
}

bool stopAll(tWorkQueueList& workQueueList)
{
    bool result = true;
    auto startWorkQueue = [&](tWorkQueueList::reference workQueue) {
        result = result && workQueue.stop();
    };
    std::for_each(workQueueList.begin(), workQueueList.end(), startWorkQueue);
    return result;
}

bool stopAllFromInternalThread(tWorkQueueList& workQueueList)
{
    bool result = true;
    auto pushStop = [&](tWorkQueueList::reference workQueue){
        result = result && workQueue.stop();
    };
    std::for_each(workQueueList.begin(), workQueueList.end(), pushStop);
    return result;
}

void Test(std::string const& testDescription, std::function<bool()>&& test)
{
    std::cout << (test() ? "[OK] " : "[ERROR] ") << testDescription << std::endl;
}

auto main(int argc, char* argv[]) -> int
{
    Test("Creating, starting and stopping by destructor", []() {
        std::array<WorkQueue, 3> workQueueList;
        if (!startAll(workQueueList))
        {
            std::cout << "Could not create one of WorkQueue" << std::endl;
            return false;
        }
        return true;
    });

    Test("Creating, starting and stopping from queue", []() {
        std::array<WorkQueue, 3> workQueueList;
        if (!startAll(workQueueList))
        {
            std::cout << "Could not create one of WorkQueue" << std::endl;
            return false;
        }
        if (!stopAllFromInternalThread(workQueueList))
        {
            std::cout << "Could not stop one of WorkQueue" << std::endl;
            return false;
        }
        return true;
    });

    Test("Creating and starting twice", []() {
        std::array<WorkQueue, 3> workQueueList;
        if (!startAll(workQueueList))
        {
            std::cout << "Could not create one of WorkQueue" << std::endl;
            return false;
        }
        if (startAll(workQueueList))
        {
            std::cout << "Should be returned false on double start" << std::endl;
            return false;
        }
        return true;
    });

    Test("Creating and stopping without starting", [](){
        std::array<WorkQueue, 3> workQueueList;
        if (stopAll(workQueueList))
        {
            std::cout << "Should be returned false on stop without start" << std::endl;
            return false;
        }
        return true;
    });

    Test("Creating, starting and double stopping", []() {
        std::array<WorkQueue, 3> workQueueList;
        if (!startAll(workQueueList))
        {
            std::cout << "Could not create one of WorkQueue" << std::endl;
            return false;
        }
        if (!stopAll(workQueueList))
        {
            std::cout << "Could not be stopped one of WorkQueue" << std::endl;
            return false;
        }
        if (stopAll(workQueueList))
        {
            std::cout << "Should be returned false on double stop" << std::endl;
            return false;
        }
        return true;
    });

    Test("Creating, pushing and after start", [](){
        std::array<WorkQueue, 3> workQueueList;

        struct TaskContext
        {
            TaskContext(WorkQueue& workQueue)
                : workQueue(workQueue)
            {
            }
            std::promise<uint32_t> result;
            std::future<uint32_t> future = result.get_future();
            WorkQueue& workQueue;
        };

        using tTaskContext = std::array<TaskContext, 3>;
        tTaskContext taskContext{workQueueList[0],
                                 workQueueList[1],
                                 workQueueList[2]};

        std::for_each(taskContext.begin(), taskContext.end(), [](tTaskContext::reference taskContext) {
            taskContext.workQueue.pushTask([&]() {
                taskContext.result.set_value(1);
            });
        });

        if (!startAll(workQueueList))
        {
            std::cout << "Could not create one of WorkQueue" << std::endl;
            return false;
        }

        uint32_t result = 0;
        std::for_each(taskContext.begin(), taskContext.end(), [&](tTaskContext::reference taskContext) {
            result += taskContext.future.get();
        });

        if (result != 3)
        {
            std::cout << "Could not be gotten result from one of task in WorkQueue" << std::endl;
            return false;
        }
        return true;
    });

    Test("Creating, starting and after pushing", [](){
        std::array<WorkQueue, 3> workQueueList;
        if (!startAll(workQueueList))
        {
            std::cout << "Could not create one of WorkQueue" << std::endl;
            return false;
        }

        struct TaskContext
        {
            TaskContext(WorkQueue& workQueue)
                : workQueue(workQueue)
            {
            }
            std::promise<uint32_t> result;
            std::future<uint32_t> future = result.get_future();
            WorkQueue& workQueue;
        };

        using tTaskContext = std::array<TaskContext, 3>;
        tTaskContext taskContext{workQueueList[0],
                                 workQueueList[1],
                                 workQueueList[2]};

        std::for_each(taskContext.begin(), taskContext.end(), [](tTaskContext::reference taskContext) {
            taskContext.workQueue.pushTask([&]() {
                taskContext.result.set_value(1);
            });
        });

        uint32_t result = 0;
        std::for_each(taskContext.begin(), taskContext.end(), [&](tTaskContext::reference taskContext) {
            result += taskContext.future.get();
        });

        if (result != 3)
        {
            std::cout << "Could not be gotten result from one of task in WorkQueue" << std::endl;
            return false;
        }
        return true;
    });

    /// ... etc pushing on the fly, from another threads etc...
    return 0;
}
#endif

